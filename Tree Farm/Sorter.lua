local chest = peripheral.wrap ("minecraft:chest_0")
local furnace = peripheral.wrap ("minecraft:furnace_1")
local woodchest = peripheral.wrap ("minecraft:chest_2")

local function sortinput()
    for var=1,27,1 do
        if chest.getItemDetail(var) ~= nil then
            local item = chest.getItemDetail(var).name
            if item == "minecraft:birch_sapling" then
                chest.pushItems("minecraft:chest_1",var,64)
            elseif item == "minecraft:birch_log" then
                chest.pushItems("minecraft:chest_2",var,64)
            elseif item == "minecraft:stick" then
                chest.pushItems("minecraft:chest_3",var,64)
            end
        end
    end
end

local function findAnyItem(c)
    for i = 1, c.size() do
        if c.getItemDetail(i) ~= nil then
            return i
        end
    end
end

local function doFurnace()
    if furnace.getItemDetail(1) == nil then
        local slotWithWood = findAnyItem(woodchest)
        if slotWithWood ~= nil then
            furnace.pullItems("minecraft:chest_2", slotWithWood, 64, 1)
        end
    end
    if furnace.getItemDetail(2) == nil or furnace.getItemDetail(2).count < 64 then
        furnace.pullItems("minecraft:furnace_1", 3, 64, 2)
    end
    if furnace.getItemDetail(3) ~= nil then
        furnace.pushItems("minecraft:chest_6",3)
    end
end

while true do
    sortinput()
    doFurnace()
end