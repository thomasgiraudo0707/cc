local DiscordHook = require("DiscordHook")
local success, hook = DiscordHook.createWebhook("https://discord.com/api/webhooks/900342618670641172/DSNs4qjX4inayrlHysoqG69DKqUzT94kLXqA5en14f0S5TUOl-AN9WIRDp9XAZvkXwGM") 
 
local function allslotsfull()
    local sumOfAllSlots = 0
    for i = 2, 16 do 
        sumOfAllSlots = turtle.getItemCount(i) + sumOfAllSlots
    end
    if sumOfAllSlots == 64*15 then
        return true
    else
        return false
    end
end
 
local function digdirection(direction)
    if direction == "up" then
        turtle.digUp()
    elseif direction == "down" then
        turtle.digDown()
    elseif direction == "forward" then
        turtle.dig()
    end
end
 
local function throw(data, direction, alwaysMine)
    if data.name == "minecraft:diamond_ore" then
        turtle.select(2)
        digdirection(direction)
    elseif data.name == "minecraft:iron_ore" then
        turtle.select(4)
        digdirection(direction)
    elseif data.name == "minecraft:gold_ore" then
        turtle.select(7)
        digdirection(direction)
    elseif data.name == "minecraft:lapis_lazuli_ore" then
        turtle.select(10)
        digdirection(direction)
    elseif data.name == "minecraft:redstone_ore" then
        turtle.select(11)
        digdirection(direction)
    elseif data.name == "minecraft:obsidian" then
        turtle.select(13)
        digdirection(direction)
    elseif data.name == "minecraft:coal_ore" then
        turtle.select(14)
        digdirection(direction)
        if turtle.getFuelLevel() < 10000  then
            turtle.refuel(64)
        end
    else
        if alwaysMine then
            turtle.select(1)
            digdirection(direction)
            turtle.drop(64)
        end
    end
end        
        
local function mine()
    while turtle.forward() == false do 
        blockIsThere, data = turtle.inspect() 
        throw(data, "forward", true)
    end

    blockIsThere, data = turtle.inspectUp()
    throw(data, "up", false)

    blockIsThere, data = turtle.inspectDown()
    throw(data, "down", false)

    turtle.turnLeft()
    blockIsThere, data = turtle.inspect()
    throw(data, "forward", false)

    turtle.turnRight()
    turtle.turnRight()
    blockIsThere, data = turtle.inspect()
    throw(data, "forward", false)

    turtle.turnLeft()
end
-- d = distance travelled
local d = 0
 
while d < turtle.getFuelLevel() and allslotsfull() == false do 
    mine()
    d = d + 1
end
 
turtle.turnRight()
turtle.turnRight()
 
for i = 1, d do
    while turtle.forward() == false do 
        blockIsThere, data = turtle.inspect()
        throw(data)
        turtle.dig()
    end
end
 
hook.send("im back in the overworld")